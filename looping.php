<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
    echo "<h3>Contoh 1</h3>";
    echo "<h4>Looping 1</h4>";
    for($i=2; $i<=20; $i+=2){
        echo $i . " - I Love PHP <br>";
    }
    echo "<h4>Looping 2</h4>";
    for($a=20; $a>=2; $a-=2){
        echo $a . " - I Love PHP <br>";
    }

    echo "<h3>Contoh 2</h3>";
    $numbers = [18, 45, 29, 61, 47, 34];
    echo "Array Numbers: ";
    print_r($numbers);
    echo "<br>";
    echo "Hasil sisa baginya adalah: ";
    foreach($numbers as $value){
        $rest[] = $value %6;
    }
    print_r($rest);

    echo "<h3>Contoh 3</h3>";
    /*
        Output:
        Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg )
        Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg )
        Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg )
        Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg )

    */

    $items = [
         ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
         ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
         ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
         ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"],
    ];

    foreach ($items as $key => $value){
        $item = array(
            'id' => $value[0],
            'Name' => $value[1],
            'Price' => $value[2],
            'Description' => $value[3],
            'Source' => $value[4]
            
        );
        print_r($item);
        echo "<br>";
    }

    echo "<h3>Contoh 4</h3>";
    for($j=1; $j<=5; $j++){
        for($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
    
   ?> 

</body>
</html>